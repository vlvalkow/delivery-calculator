<?php

namespace App\DeliveryCalculator\DeliveryTimes;

use App\DeliveryCalculator\Contracts\DeliveryTimeInterface;
use App\Models\ShippingZone;

class CountryDeliveryTime implements DeliveryTimeInterface
{
    public function getTime(ShippingZone $shippingZone, string $location): int
    {
        if ($shippingZone->code !== $location) {
            return false;
        }

        return $shippingZone->delivery_time;
    }
}

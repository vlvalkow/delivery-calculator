<?php

namespace App\DeliveryCalculator\DeliveryTimes;

use App\DeliveryCalculator\Contracts\DeliveryTimeInterface;
use App\Models\ShippingZone;
use GuzzleHttp\Client;

class RegionDeliveryTime implements DeliveryTimeInterface
{
    public function getTime(ShippingZone $shippingZone, string $location): int
    {
        if ($shippingZone->code !== $this->getRegionForLocation($location)) {
            return false;
        }

        return $shippingZone->delivery_time;
    }

    private function getRegionForLocation(string $location): string
    {
        $client = new Client();

        $guzzleResponse = $client->get('https://restcountries.eu/rest/v2/alpha/' . $location);

        $restCountriesResponse = json_decode($guzzleResponse->getBody()->getContents());

        return $restCountriesResponse->region;
    }
}

<?php

namespace App\DeliveryCalculator\DeliveryTimes;

use App\DeliveryCalculator\Contracts\DeliveryTimeInterface;
use App\Models\ShippingZone;

class WorldWideDeliveryTime implements DeliveryTimeInterface
{
    public function getTime(ShippingZone $shippingZone, string $location): int
    {
        return $shippingZone->delivery_time;
    }
}

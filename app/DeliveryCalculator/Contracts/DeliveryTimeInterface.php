<?php

namespace App\DeliveryCalculator\Contracts;

use App\Models\ShippingZone;

interface DeliveryTimeInterface
{
    public function getTime(ShippingZone $shippingZone, string $location): int;
}

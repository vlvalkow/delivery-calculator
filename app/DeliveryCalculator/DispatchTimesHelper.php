<?php

namespace App\DeliveryCalculator;

use Carbon\Carbon;
use Cmixin\BusinessTime;

class DispatchTimesHelper
{
    protected $dispatchHours;

    public function __construct(BusinessTime $dispatchHours)
    {
        $this->dispatchHours = $dispatchHours::enable(Carbon::class, [
            'monday' => ['00:00-16:00'],
            'tuesday' => ['00:00-16:00'],
            'wednesday' => ['00:00-16:00'],
            'thursday' => ['00:00-16:00'],
            'friday' => ['00:00-16:00'],
        ]);
    }

    public function isDispatchingNow(Carbon $datetime): int
    {
        return $datetime->isOpen();
    }

    public function getNextBusinessDay(Carbon $datetime): Carbon
    {
        return $datetime->nextOpen();
    }
}

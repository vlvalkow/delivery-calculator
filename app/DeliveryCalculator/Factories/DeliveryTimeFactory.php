<?php

namespace App\DeliveryCalculator\Factories;

use App\DeliveryCalculator\Contracts\DeliveryTimeInterface;
use App\DeliveryCalculator\DeliveryTimes\CountryDeliveryTime;
use App\DeliveryCalculator\DeliveryTimes\RegionDeliveryTime;
use App\DeliveryCalculator\DeliveryTimes\WorldWideDeliveryTime;
use App\Models\ShippingZone;

class DeliveryTimeFactory
{
    public function getDeliveryTimeStrategy(ShippingZone $shippingZone): DeliveryTimeInterface
    {
        switch ($shippingZone->type) {
            case 'country':
                return new CountryDeliveryTime();
            case 'region':
                return new RegionDeliveryTime();
            case 'worldwide':
                return new WorldWideDeliveryTime();
            default:
                throw new \RuntimeException('Shipping zone is not supported.');
        }
    }
}

<?php

namespace App\DeliveryCalculator;

use App\DeliveryCalculator\Factories\DeliveryTimeFactory;
use App\Models\ShippingMethod;
use Carbon\Carbon;

class DeliveryCalculator
{
    protected $deliveryTimeFactory;
    protected $dispatchTimesHelper;

    public function __construct(DeliveryTimeFactory $deliveryTimeFactory, DispatchTimesHelper $dispatchTimesHelper)
    {
        $this->deliveryTimeFactory = $deliveryTimeFactory;
        $this->dispatchTimesHelper = $dispatchTimesHelper;
    }

    public function calculateDeliveryDate(ShippingMethod $shippingMethod, string $location, Carbon $datetime): ?Carbon
    {
        $deliveryTime = $this->getShippingZoneDeliveryTime($shippingMethod, $location);

        if ($deliveryTime === null) {
            return $deliveryTime;
        }

        $dispatchDate = $this->getDispatchDate($datetime);

        return $dispatchDate->addBusinessDays($deliveryTime);
    }

    protected function getShippingZoneDeliveryTime(ShippingMethod $shippingMethod, $location): ?int
    {
        foreach ($shippingMethod->shippingZones as $shippingZone) {
            $deliveryTimeStrategy = $this->deliveryTimeFactory->getDeliveryTimeStrategy($shippingZone);
            $deliveryTime = $deliveryTimeStrategy->getTime($shippingZone, $location);

            if (!empty($deliveryTime)) {
                return $deliveryTime - 1;
            }
        }

        return null;
    }

    protected function getDispatchDate(Carbon $datetime): Carbon
    {
        if (!$this->dispatchTimesHelper->isDispatchingNow($datetime)) {
            return $this->dispatchTimesHelper->getNextBusinessDay($datetime);
        }

        return $datetime;
    }
}

<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ShippingMethod extends Model
{
    public function shippingZones()
    {
        return $this->belongsToMany(ShippingZone::class);
    }
}

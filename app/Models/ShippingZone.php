<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ShippingZone extends Model
{
    public function shippingMethods()
    {
        return $this->belongsToMany(ShippingMethod::class);
    }
}

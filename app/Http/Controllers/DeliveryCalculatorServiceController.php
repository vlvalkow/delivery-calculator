<?php

namespace App\Http\Controllers;

use App\DeliveryCalculator\DeliveryCalculator;
use App\Http\Requests\CalculateDeliveryDateRequest;
use App\Models\ShippingMethod;
use App\Models\ShippingZone;
use Carbon\Carbon;

class DeliveryCalculatorServiceController extends Controller
{
    /**
     * @var DeliveryCalculator $deliveryCalculator
     */
    private $deliveryCalculator;

    public function __construct(DeliveryCalculator $deliveryCalculator)
    {
        $this->deliveryCalculator = $deliveryCalculator;
    }

    public function calculateDate(CalculateDeliveryDateRequest $request)
    {
        $shippingMethod = factory(ShippingMethod::class)->make();
        $shippingZoneCountry = factory(ShippingZone::class)->state('country')->make();
        $shippingZoneRegion = factory(ShippingZone::class)->state('region')->make();
        $shippingZoneWorldWide = factory(ShippingZone::class)->state('worldwide')->make();

        $shippingMethod->shippingZones = collect([$shippingZoneCountry, $shippingZoneRegion, $shippingZoneWorldWide]);

        $location = $request->get('location');
        $datetime = Carbon::parse($request->get('datetime'));

        $deliveryDateEstimate = $this->deliveryCalculator->calculateDeliveryDate($shippingMethod, $location, $datetime);

        if ($deliveryDateEstimate instanceof Carbon) {
            $deliveryDateEstimate = $deliveryDateEstimate->format('l, jS \o\f F Y');
        }

        return response()
            ->json([
                'delivery_date' => $deliveryDateEstimate
            ]);
    }
}

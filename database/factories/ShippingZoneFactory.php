<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\ShippingZone::class, function() {
    return [];
});

$factory->state(\App\Models\ShippingZone::class, 'country', [
        'type' => 'country',
        'code' => 'GB',
        'name' => 'United Kingdom',
        'delivery_time' => 1
]);

$factory->state(\App\Models\ShippingZone::class, 'region', [
    'type' => 'region',
    'code' => 'Europe',
    'name' => 'Europe',
    'delivery_time' => 3
]);

$factory->state(\App\Models\ShippingZone::class, 'worldwide', [
    'type' => 'worldwide',
    'code' => 'WW',
    'name' => 'Worldwide',
    'delivery_time' => 8
]);

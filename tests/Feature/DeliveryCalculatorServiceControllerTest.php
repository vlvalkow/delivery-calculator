<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class DeliveryCalculatorServiceControllerTest extends TestCase
{
    public function test_service_returns_delivery_date()
    {
        $carbonDate = Carbon::create('2019', '06', '08', '15', '04');

        $response = $this->json('GET', '/api/calculate/delivery/date', [
            'shipping_method' => 'royal_mail',
            'location' => 'GB',
            'datetime' => $carbonDate->toDateTimeString(),
        ]);

        $response
            ->assertStatus(200)
            ->assertSee('delivery_date');
    }
}

<?php

namespace Tests\Feature;

use App\DeliveryCalculator\DeliveryCalculator;
use App\Models\ShippingMethod;
use App\Models\ShippingZone;
use Carbon\Carbon;
use Tests\TestCase;

class DeliveryCalculatorTest extends TestCase
{
    /**
     * @var $calculator DeliveryCalculator
     */
    public $calculator;

    public function setUp(): void
    {
        $this->calculator = app()->make('App\DeliveryCalculator\DeliveryCalculator');

        parent::setUp();
    }

    public function test_calculate_delivery_date()
    {
        $shippingMethod = factory(ShippingMethod::class)->make();
        $shippingZone = factory(ShippingZone::class)->state('country')->make();

        $shippingMethod->shippingZones = collect([$shippingZone]);

        /**
         * @var $estimate Carbon
         */
        $datetime = Carbon::create('2019-06-08T15:04:00');
        $estimate = $this->calculator->calculateDeliveryDate($shippingMethod, 'GB', $datetime);

        $this->assertEquals($shippingZone->delivery_time, $estimate->diffInDays($datetime) + 1);
    }
}

module.exports = () => {
    const fontawesome = require('@fortawesome/fontawesome')
        // Solid
        , faSpinner = require('@fortawesome/fontawesome-free-solid/faSpinner')

        // Regular
        , faCalendarAlt = require('@fortawesome/fontawesome-free-regular/faCalendarAlt')
    ;
    fontawesome.library.add(faSpinner);

    fontawesome.library.add(faCalendarAlt);
};

# Setup
To setup this project please run the following commands:

1. git clone git@bitbucket.org:vlvalkow/delivery-calculator.git
2. cd delivery-calculator
3. composer install
4. npm install
5. npm run dev
6. phpunit (optional, for running unit tests)

# Notes
Data about shipping zones and methods is generated via model factories. Data about business days and times is hardcoded in the App\DeliveryCalculator\DispatchTimesHelper class.
